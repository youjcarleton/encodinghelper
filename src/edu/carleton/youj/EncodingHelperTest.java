package edu.carleton.youj;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import static org.junit.Assert.*;

/**
 * Created by Josh on 4/12/2015.
 */
public class EncodingHelperTest {

    @Test
    public void testFindType() throws Exception {
        String[] args = {"--input", "codepoint", "--output", "string",
                "U+0041"};
        assertEquals(4, EncodingHelper.findType(args, 0, 3)[0]);
        assertEquals(2, EncodingHelper.findType(args, 0, 3)[1]);
        assertEquals(0, EncodingHelper.findType(args, 0, 3)[2]);
    }

    @Test
    public void testFindTypeShouldAcceptShorthand() throws Exception {
        String[] args = {"-i", "utf8", "-o", "codepoint",
                "\\x41"};
        assertEquals(4, EncodingHelper.findType(args, 0, 3)[0]);
        assertEquals(1, EncodingHelper.findType(args, 0, 3)[1]);
        assertEquals(2, EncodingHelper.findType(args, 0, 3)[2]);
    }

    @Test
    public void testFindTypeShouldAcceptReverseOrder() throws Exception {
        String[] args = {"-o", "summary", "-i", "string",
                "hi"};
        assertEquals(4, EncodingHelper.findType(args, 0, 3)[0]);
        assertEquals(0, EncodingHelper.findType(args, 0, 3)[1]);
        assertEquals(3, EncodingHelper.findType(args, 0, 3)[2]);
    }

    @Test
    public void testFindTypeShouldAcceptOneSpecification() throws Exception {
        String[] args = {"-i", "string", "hi"};
        assertEquals(2, EncodingHelper.findType(args, 0, 3)[0]);
        assertEquals(0, EncodingHelper.findType(args, 0, 3)[1]);
        assertEquals(3, EncodingHelper.findType(args, 0, 3)[2]);
    }

    @Test
    public void testProcessString() throws Exception {
        EncodingHelperChar[] testlist = new EncodingHelperChar[4];
        testlist[0] = new EncodingHelperChar('�');
        testlist[1] = new EncodingHelperChar('t');
        testlist[2] = new EncodingHelperChar('r');
        testlist[3] = new EncodingHelperChar('e');
        for (int i = 0; i < 4; i++) {
            assertEquals(EncodingHelper.processString("�tre")[i].getCodePoint(),
                    testlist[i].getCodePoint());
        }
    }

    @Test
    public void testProcessUtf() throws Exception {
        EncodingHelperChar[] testlist = new EncodingHelperChar[4];
        testlist[0] = new EncodingHelperChar('a');
        testlist[1] = new EncodingHelperChar('b');
        testlist[2] = new EncodingHelperChar('c');
        testlist[3] = new EncodingHelperChar(0x00EA);
        for (int i = 0; i < 4; i++) {
            assertEquals(EncodingHelper.processUtf(
                            "\\x61\\x62\\x63\\xC3\\xAA")[i].getCodePoint(),
                    testlist[i].getCodePoint());
        }
    }

    @Test
    public void testProcessCodePointForMultipleArguments() throws Exception {
        EncodingHelperChar[] testlist = new EncodingHelperChar[4];
        testlist[0] = new EncodingHelperChar(0x00EA);
        testlist[1] = new EncodingHelperChar('t');
        testlist[2] = new EncodingHelperChar('r');
        testlist[3] = new EncodingHelperChar('e');
        String[] args = {"U+00EA", "U+0074", "U+0072", "U+0065"};
        for (int i = 0; i < 4; i++) {
            assertEquals(
                    EncodingHelper.processCodepoint(args, 0)[i].getCodePoint(),
                    testlist[i].getCodePoint());
        }
    }

    @Test
    public void testProcessCodePointForLongString() throws Exception {
        EncodingHelperChar[] testlist = new EncodingHelperChar[4];
        testlist[0] = new EncodingHelperChar(0x00EA);
        testlist[1] = new EncodingHelperChar('t');
        testlist[2] = new EncodingHelperChar('r');
        testlist[3] = new EncodingHelperChar('e');
        String[] args = {"U+00EA U+0074 U+0072 U+0065"};
        for (int i = 0; i < 4; i++) {
            assertEquals(
                    EncodingHelper.processCodepoint(args, 0)[i].getCodePoint(),
                    testlist[i].getCodePoint());
        }
    }

    @Test
    public void testInferType() throws Exception {
        String[] args = {"\\xC3\\xAA"};
        assertEquals(1, EncodingHelper.inferType(0, args));
    }

    @Test
    public void testInferTypeShouldRecognizeCodepoint() throws Exception {
        String[] args = {"U+0041"};
        assertEquals(2, EncodingHelper.inferType(0, args));
    }

    @Test
    public void testFormatString() throws Exception {
        EncodingHelperChar[] charlist = new EncodingHelperChar[3];
        charlist[0] = new EncodingHelperChar('a');
        charlist[1] = new EncodingHelperChar('b');
        charlist[2] = new EncodingHelperChar('c');
        assertEquals("abc", EncodingHelper.formatString(charlist));
    }

    @Test
    public void testFormatUTF() throws Exception {
        EncodingHelperChar[] charlist = new EncodingHelperChar[3];
        charlist[0] = new EncodingHelperChar('a');
        charlist[1] = new EncodingHelperChar('b');
        charlist[2] = new EncodingHelperChar('c');
        assertEquals("\\x61\\x62\\x63", EncodingHelper.formatUTF(charlist));
    }

    @Test
    public void testFormatCodePoint() throws Exception {
        EncodingHelperChar[] charlist = new EncodingHelperChar[3];
        charlist[0] = new EncodingHelperChar('a');
        charlist[1] = new EncodingHelperChar('b');
        charlist[2] = new EncodingHelperChar('c');
        assertEquals("U+0061 U+0062 U+0063 ",
                EncodingHelper.formatCodePoint(charlist));
    }
}