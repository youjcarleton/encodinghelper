package edu.carleton.youj;

import java.io.IOException;
import java.util.*;


/**
 * Created by Youj and Subramaniana on 4/12/2015.
 */
public class EncodingHelper {
    public static void main(String[] args) {
        /* input values: 0 = string
        1 = utf-8
        2 = codepoint
        Output values:
        0 = string
        1 = utf-8
        2 = codepoint
        3 = summary
         */
        int input = 0;
        int output = 3;
        int startvalue = 0;
        //As standard, if user doesn't add an input we return help for them.
        if(args.length == 0){
            printUsage();
            return;
        }
        //tries to infer the type of the input
        input = inferType(startvalue, args);

        /*if argument is of length 1, it skips the process of checking for extra
        arguments and goes straight to printing, if the input is a string.
        */
        if(args.length == 1){
            if (input == 0 ) {
                if (args[0].length() == 1) {
                    printSummaryChar(args[0].charAt(0));
                    return;
                } else {
                    printSummaryString(args[0]);
                    return;
                }
            }

        } else {
            int[] types = findType(args, input, output);
            startvalue = types[0];
            input = types[1];
            output = types[2];
        }
        /*
        Read input and make it an array of:
        strings if characters if string input
         bytes is utf 8 input,
         and ints if codepoint.
        Loop through the array and create encodinghelperchar objects each time.
        we then call the method
        corresponding to the type of output the user wants
        and prints out what we need to print.
         */

        //string inputs
        if (input == 0){
            outputControl(processString(args[startvalue]), output);
        } else if (input == 1){
            if (inferType(startvalue, args) != 1) {
                printUsage();
            }
            outputControl(processUtf(args[startvalue]), output);
            //code for Unicode inputs.
        } else if (input == 2){
            if (inferType(startvalue, args) != 2) {
                printUsage();
            }
            outputControl(processCodepoint(args, startvalue), output);
        }
    }
    //usage statements explains what's going on.
    public static void printUsage() {
        System.out.println("Usage: java EncodingHelper [--input <type>] " +
                "[--output <type>] <data>");
        System.out.println("input types: string, utf8, codepoint. output " +
                "types: " + "string, utf8, codepoint, summary.");
        System.out.println("Codepoints must be formatted U+#### " +
                "in hexadecimal.");
        System.out.println("Multiple codepoints accepted; separate by space");
        System.out.println("utf8 bytes must be formatted '\\x##\\x##");
        System.out.println("Bytes can be entered in any quantity");
        System.exit(0);
    }


    /*
    If the passed argument that argument has the requisite length and fits
    conditions to be UTF8 bytes or codepoints, they are read as such.
    */
    public static int inferType(int startvalue, String[] arg) {

        if ((arg[startvalue].length() > 1) &&
                (arg[startvalue].charAt(0) == '\\') &&
                (arg[startvalue].charAt(1) == 'x')) {
            return 1;
        } else if ((arg[startvalue].length() > 1) &&
                (arg[startvalue].charAt(0) == 'U') &&
                (arg[startvalue].charAt(1) == '+')) {
            return 2;
        }
        return 0;
    }
    /*
        Here we check for all the possible arguments regarding input and output
        possibilities. Invalid argument errors are produced if the user
        types in something which doesn't make sense. After that, the help
        function is printed. A second reading of the input occurs after output
        type is specified, as we only checked the first argument earlier.
         */

    public static int[] findType(String[] args, int input, int output) {
        int startvalue = 0;
        try {
            if (args[0].equals("-o") | args[0].equals("--output")) {
                startvalue = 2;
                if (args[1].equals("string")) {
                    output = 0;
                } else if (args[1].equals("utf8")) {
                    output = 1;
                } else if (args[1].equals("codepoint")) {
                    output = 2;
                } else if (args[1].equals("summary")) {
                    output = 3;
                } else {
                    System.out.println("Invalid output type");
                    printUsage();
                }
                input = inferType(startvalue, args);

            } else if (args[0].equals("-i") | args[0].equals("--input")) {
                startvalue = 2;
                if (args[1].equals("string")) {
                    input = 0;
                } else if (args[1].equals("utf8")) {
                    input = 1;
                } else if (args[1].equals("codepoint")) {
                    input = 2;
                } else {
                    System.out.println("Invalid input type");
                    printUsage();
                }
            } else {
                System.out.println("Invalid first argument");
                printUsage();
            }
            if (args[2].equals("-o") | args[2].equals("--output")) {
                startvalue = 4;
                if (args[3].equals("string")) {
                    output = 0;
                } else if (args[3].equals("utf8")) {
                    output = 1;
                } else if (args[3].equals("codepoint")) {
                    output = 2;
                } else if (args[3].equals("summary")) {
                    output = 3;
                } else {
                    System.out.println("Invalid output type");
                    printUsage();
                }
            } else if (args[2].equals("-i") | args[2].equals("--input")) {
                startvalue = 4;
                if (args[3].equals("string")) {
                    input = 0;
                } else if (args[3].equals("utf8")) {
                    input = 1;
                } else if (args[3].equals("codepoint")) {
                    input = 2;
                } else {
                    System.out.println("Invalid input type");
                    printUsage();
                }
            }
            return new int[] {startvalue, input, output};
        } catch (java.lang.IndexOutOfBoundsException e) {
            printUsage();
            return new int[] {startvalue, input, output};
        }
    }

    public static EncodingHelperChar[] processString(String s) {
        char[] chars = s.toCharArray();
        EncodingHelperChar[] charlist =
                new EncodingHelperChar[chars.length];
        for(int i = 0; i < chars.length; i++){
            charlist[i] = new EncodingHelperChar(chars[i]);
        }
        return charlist;
    }

    public static EncodingHelperChar[] processUtf(String s) {
        String stripped = s.replace("\\", "");
        stripped = stripped.replace("'", "");
        String[] split = stripped.split("x");
        ArrayList<EncodingHelperChar> charlist =
                new ArrayList<EncodingHelperChar>();
        byte[] bytearray  = new byte[split.length];
        for(int i = 1; i < split.length; i++){
            bytearray[i] = (byte) Integer.parseInt(split[i], 16);
        }
        int point = 1;
        try {
                /*Here we check each byte to see if its the first byte in the
                sequence. We then store the number of bytes ascribed by the
                first byte to a byte array and create a encodinghelperchar
                object from that.
                 */
            byte[] a = new byte[1];
            byte[] b = new byte[2];
            byte[] c = new byte[3];
            byte[] d = new byte[4];
            while (point < bytearray.length) {
                if ((bytearray[point] & (byte) 0b10000000) ==
                        (byte) 0b00000000) {

                    a[0] = bytearray[point];
                    charlist.add(new EncodingHelperChar(a));
                    point++;
                } else if ((bytearray[point] & (byte) 0b11100000) ==
                        (byte) 0b11000000){
                    b[0] = bytearray[point];
                    b[1] = bytearray[point + 1];
                    charlist.add(new EncodingHelperChar(b));
                    point = point + 2;
                } else if((bytearray[point] & (byte) 0b11110000) ==
                        (byte) 0b11100000){
                    c[0] = bytearray[point];
                    c[1] = bytearray[point + 1];
                    c[2] = bytearray[point + 2];
                    charlist.add(new EncodingHelperChar(c));
                    point = point + 3;
                } else {
                    d[0] = bytearray[point];
                    d[1] = bytearray[point + 1];
                    d[2] = bytearray[point + 2];
                    d[3] = bytearray[point + 3];
                    charlist.add(new EncodingHelperChar(d));
                    point = point + 4;
                }
            }
            EncodingHelperChar[] charArray =
                    new EncodingHelperChar[charlist.size()];
            charArray = charlist.toArray(charArray);
            return charArray;
        } catch (java.lang.IllegalArgumentException |
                java.lang.IndexOutOfBoundsException e){
            System.out.print("Invalid byte input");
            printUsage();
            return null;
        }
    }

    public static EncodingHelperChar[] processCodepoint(String[] args,
                                                        int startvalue) {

        if(args.length > startvalue + 1){
            EncodingHelperChar[] charlist =
                    new EncodingHelperChar[args.length - startvalue];
            for (int i = startvalue; i < args.length; i++) {
                int inputInt =
                        Integer.parseInt(args[i].split("\\+")[1], 16);
                charlist[i - startvalue] = new EncodingHelperChar(inputInt);
            }
            return charlist;
        } else {
            String[] codepointArray = args[startvalue].split(" ");
            EncodingHelperChar[] charlist =
                    new EncodingHelperChar[codepointArray.length];
            for (int i = 0; i < codepointArray.length; i++) {
                int inputInt = Integer.parseInt(
                        codepointArray[i].split("\\+")[1], 16);
                charlist[i] = new EncodingHelperChar(inputInt);
            }
            return charlist;
        }
    }
    //Calls a function depending on what the output value is.
    public static void outputControl(EncodingHelperChar[] charlist, int output)
    {
        if (output == 0) {
            System.out.println(formatString(charlist));
        } else if (output == 1) {
            System.out.println(formatUTF(charlist));
        } else if (output == 2) {
            System.out.println(formatCodePoint(charlist));
        } else {
            printSummaryArray(charlist);
        }
    }
    //prints the summary if the input is just one character
    public static void printSummaryChar(char ch) {

        try {
            EncodingHelperChar a = new EncodingHelperChar(ch);
            System.out.println("Character: " + (char) a.getCodePoint());
            System.out.println("Code point: " + a.toCodePointString());
            System.out.println("Name: " + a.getCharacterName());
            System.out.println("UTF-8: " + a.toUtf8String());
        } catch (IOException e) {
            System.out.println("invalid character inputted");
        }
    }
    //prints out summary for a string input
    public static void printSummaryString(String s) {
        System.out.println("String: " + s);
        EncodingHelperChar a = new EncodingHelperChar(0);
        String codePoints = "Code points: ";
        String bytes = "UTF-8: ";
        for (int i = 0; i < s.length(); i++) {
            char ch = s.charAt(i);
            a.setCodePoint((int) ch);
            codePoints += a.toCodePointString() + " ";
            bytes += a.toUtf8String();
        }
        System.out.println(codePoints);
        System.out.println(bytes);
    }
    //formats string for Encodinghelper inputs
    public static String formatString(EncodingHelperChar[] charlist) {
        String toPrint = "";
        for (int i = 0; i < charlist.length; i++) {
            toPrint += ((char) charlist[i].getCodePoint());
        }
        return toPrint;
    }
    //formats utf8 output
    public static String formatUTF(EncodingHelperChar[] charlist) {
        String toPrint = "";
        for (int i = 0; i < charlist.length; i++) {
            toPrint += (charlist[i].toUtf8String());
        }
        return toPrint;
    }
    //formats codepoint output
    public static String formatCodePoint(EncodingHelperChar[] charlist) {
        String toPrint = "";
        for (int i = 0; i < charlist.length; i++) {
            toPrint += (charlist[i].toCodePointString() + " ");
        }
        return toPrint;
    }
    //prints summary if input had arguments
    public static void printSummaryArray(EncodingHelperChar[] charlist) {
        if (charlist.length == 1) {
            printSummaryChar((char) charlist[0].getCodePoint());
        } else {
            String s = "";
            for (int i = 0; i < charlist.length; i++) {
                s += (char) charlist[i].getCodePoint();
            }
            printSummaryString(s);
        }
    }
}