package edu.carleton.youj;

import org.junit.Test;

import java.lang.*;

import static org.junit.Assert.*;

/**
 * Created by Josh and Aditya on 4/5/2015.
 */
public class EncodingHelperCharTest {
    @org.junit.Test
    public void testEncodingHelperCharConstructorInt() throws Exception {
        EncodingHelperChar testClass = new EncodingHelperChar(62);
        assertEquals(testClass.getCodePoint(), 62);
    }

    @org.junit.Test
    public void testEncodingHelperCharConstructorByte() throws Exception {
        byte[] a = {(byte) 0xF4, (byte) 0x8F, (byte) 0xBF, (byte) 0xBF};
        EncodingHelperChar testClass = new EncodingHelperChar(a);
        assertEquals("U+10FFFF", testClass.toCodePointString());
    }

    @org.junit.Test
    public void testEncodingHelperCharConstructorByteShouldNotBeTooLarge() throws Exception {
        byte[] a = {-12, -113, -65, -64};
        EncodingHelperChar testClass = new EncodingHelperChar(a);
        assertEquals(0, testClass.getCodePoint());
        System.out.print(testClass.getCodePoint());
    }

    @org.junit.Test
    public void testEncodingHelperCharConstructorChar() throws Exception {
        char ch = 'A';
        EncodingHelperChar testClass = new EncodingHelperChar(ch);
        assertEquals(0x0041, testClass.getCodePoint());
    }

    @org.junit.Test
    public void testGetCodePoint() throws Exception {
        EncodingHelperChar testClass = new EncodingHelperChar(0x0041);
        assertEquals(0x0041, testClass.getCodePoint());
    }

    @org.junit.Test
    public void testSetCodePoint() throws Exception {
        EncodingHelperChar testClass = new EncodingHelperChar(41);
        testClass.setCodePoint(1);
        assertEquals(1, testClass.getCodePoint());
    }

    @org.junit.Test
    public void testSetCodePointShouldNotBeNegative() throws Exception {
        //Check for Negative
        try {
            EncodingHelperChar testClass = new EncodingHelperChar(41);
            testClass.setCodePoint(-1);
            fail("exception thrown: no negative codepoints allowed");
        } catch (java.lang.IllegalArgumentException e){}
    }

    @org.junit.Test
    public void testSetCodePointShouldNotBeTooLarge() throws Exception {
        //Check for Too Large numbers
        try {
            EncodingHelperChar testClass = new EncodingHelperChar(41);
            testClass.setCodePoint(0x110000);
            fail("number too large for codepoint");
        } catch (java.lang.IllegalArgumentException e) {}

    }

    @org.junit.Test
    public void testToUtf8Bytes() throws Exception {
        char ch = 'A';
        EncodingHelperChar testClass = new EncodingHelperChar('�');
        byte[] a = {(byte) 0xC2, (byte) 0xA2};
        EncodingHelperChar testClass1 = new EncodingHelperChar(a);
        assertEquals(testClass.getCodePoint(), testClass1.getCodePoint());
        //assertEquals(a, testClass.toUtf8Bytes());
    }

    @org.junit.Test
    public void testToCodePointString() throws Exception {
        EncodingHelperChar testClass = new EncodingHelperChar(0x0041);
        assertEquals("U+0041", testClass.toCodePointString());
    }

    @org.junit.Test
    public void testToUtf8String() throws Exception {
        EncodingHelperChar testClass = new EncodingHelperChar(0xFFEE);
        assertEquals("\\xEF\\xBF\\xAE", testClass.toUtf8String());
    }

    @org.junit.Test
    public void testGetCharacterName() throws Exception {
        EncodingHelperChar testClass = new EncodingHelperChar(0x7F);
        assertEquals("<control> DELETE", testClass.getCharacterName());
    }
}