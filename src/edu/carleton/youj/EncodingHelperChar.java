/**
 * The main model class for the EncodingHelper project in
 * CS 257, Spring 2015, Carleton College. Each object of this class
 * represents a single Unicode character. The class's methods
 * generate various representations of the character. 
 */

package edu.carleton.youj;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.UnsupportedEncodingException;


public class EncodingHelperChar {


    private int codePoint;

    /**
     * Constructs a new instance of EncodingHelperChar.
     *Takes in an integer and sets it as codePoint. Checks to make sure int is
     * within range and throws error if not.
     * @param codePoint set as new codePoint
     * @throws java.lang.IllegalArgumentException
     */
    public EncodingHelperChar(int codePoint) throws
            java.lang.IllegalArgumentException {
        this.codePoint = codePoint;
        //check to see if codePoint is not negative.
        if (this.codePoint < 0) {
            this.codePoint = 0;

            throw new java.lang.IllegalArgumentException("codepoint cannot be" +
                    "negative, set to 0");

        }
        //checks to see if codePoint is not too large.
        if (this.codePoint > 0x10FFFF) {

            this.codePoint = 0;
            throw new java.lang.IllegalArgumentException("codepoint cannot be" +
                    "that large, set to 0");

        }

    }

    /**
     * Creates a new instance of EncodingHelperChar.
     * Takes in a byte array as input and sets the corresponding codePoint as
     * the stored codePoint.
     * Throws IllegalArgumentException if codepoint is too large or byte array
     * is invalid.
     * @param utf8Bytes bytes set as new codePoint
     * @throws java.lang.IllegalArgumentException
     */
    public EncodingHelperChar(byte[] utf8Bytes) throws
            java.lang.IllegalArgumentException {
        try {
            if ((utf8Bytes[0] & (byte) 0b10000000) == (byte) 0b00000000) {
                //1 byte array
                if (utf8Bytes.length > 1) {
                    throw new java.lang.IllegalArgumentException("");
                }
                this.codePoint = (int) utf8Bytes[0];
            } else {
                if ((utf8Bytes[0] & (byte) 0b11100000) == (byte) 0b11000000) {
                    if (utf8Bytes.length > 2) {
                        throw new java.lang.IllegalArgumentException("");
                    }
                    //two byte array
                    //gets last 5 bits of first byte.
                    byte temp1 = (byte) (0b00011111 & utf8Bytes[0]);
                    //gets last 6 bits of second byte
                    byte temp2 = (byte) (0b00111111 & utf8Bytes[1]);
                    //sets codepoint by combining temp1 and 2.
                    this.codePoint = (temp2 | (temp1 << 6));
                } else {
                    if((utf8Bytes[0] & (byte) 0b11110000) == (byte) 0b11100000){
                        if (utf8Bytes.length > 3) {
                            throw new java.lang.IllegalArgumentException("");
                        }
                        //three byte array
                        //gets last 5 bits of first byte.
                        byte temp1 = (byte) (0b00001111 & utf8Bytes[0]);
                        //get last 6 bits of next two bytes.
                        byte temp2 = (byte) (0b00111111 & utf8Bytes[1]);
                        byte temp3 = (byte) (0b00111111 & utf8Bytes[2]);
                        //combines the three to set codePoint.
                        this.codePoint = (temp1 << 12 | temp2 << 6) | temp3;
                    } else {
                        //4 byte array
                        if (utf8Bytes.length > 4) {
                            throw new java.lang.IllegalArgumentException("Max "
                                    + "number of bytes is 4");
                        }
                        //gets last 4 bits of first byte.
                        byte temp1 = (byte) (0b00000111 & utf8Bytes[0]);
                        //gets last 6 bits of next three bytes.
                        byte temp2 = (byte) (0b00111111 & utf8Bytes[1]);
                        byte temp3 = (byte) (0b00111111 & utf8Bytes[2]);
                        byte temp4 = (byte) (0b00111111 & utf8Bytes[3]);
                        //combines the temp vars to set Codepoint.
                        this.codePoint = ((temp1 << 18 |
                                temp2 << 12) | temp3 << 6) | temp4;
                    }
                }
            }
            //if index is out of bounds, there was an illegal argument.
        } catch (IndexOutOfBoundsException e){
            throw new java.lang.IllegalArgumentException("");
        }
    }


    /**
     * Creates new instance of EncodingHelperChar with the codePoint of the
     * character saved as the codepoint stored.
     * @param ch set as new codepoint
     */
    public EncodingHelperChar(char ch) {
        this.codePoint = (int) ch;
    }

    /**
     *
     * @return Returns the stored Codepoint as an int.
     */
    public int getCodePoint() {
        return this.codePoint;
    }

    /**
     * Sets inputted codepoint as the stored codepoint.
     * @param codePoint stored as codePoint.
     * @throws java.lang.IllegalArgumentException
     */
    public void setCodePoint(int codePoint) throws
            java.lang.IllegalArgumentException {
        this.codePoint = codePoint;
        //check if inputted codepoint is negative.
        if (this.codePoint < 0) {
            this.codePoint = 0;

            throw new java.lang.IllegalArgumentException("codepoint cannot be" +
                    "negative, set to 0");

        }
        //check if inputted codepoint is too large.
        if (this.codePoint > 0x10FFFF) {

            this.codePoint = 0;
            throw new java.lang.IllegalArgumentException("codepoint cannot be" +
                    "that large, set to 0");

        }
    }

    /**
     * Converts this character into an array of the bytes in its UTF-8
     * representation.
     *   For example, if this character is a lower-case letter e with an acute
     * accent, whose UTF-8 form consists of the two-byte sequence C3 A9, then
     * this method returns a byte[] of length 2 containing C3 and A9.
     *
     * @return the UTF-8 byte array for this character
     */
    public byte[] toUtf8Bytes() {
        try{
            String s = "" + (char) this.codePoint;
            return s.getBytes("UTF-8");
        } catch (UnsupportedEncodingException e){
            System.out.println("Encoding not supported, sorry");
            return null;
        }
    }

    /**
     * Generates the conventional 4-digit hexadecimal code point notation for
     * this character.
     *   For example, if this character is a lower-case letter e with an acute
     * accent, then this method returns the string U+00E9 (no quotation marks in
     * the returned String).
     *
     * @return the U+ string for this character
     */
    public String toCodePointString() {
        String hex = Integer.toHexString(this.codePoint);
        String output = hex;
        //adds zeroes to start of hex string as needed to format correctly.
        for (int i = 0; i < 4 - hex.length(); i++) {
            output = "0" + output;
        }
        return "U+" + output.toUpperCase();
    }

    /**
     * Generates a hexadecimal representation of this character suitable for
     * pasting into a string literal in languages that support hexadecimal byte
     * escape sequences in string literals (e.g. C, C++, and Python).
     *   For example, if this character is a lower-case letter e with an acute
     * accent (U+00E9), then this method returns the string \xC3\xA9. Note that
     * quotation marks should not be included in the returned String.
     *
     * @return the escaped hexadecimal byte string
     */
    public String toUtf8String() {
        //creates a byte array use 'toUtf8Bytes' function.
        byte[] bytearray = this.toUtf8Bytes();
        String value = "";
        //create string in output format by adding required prefixes.
        for(int i = 0; i < bytearray.length; i++){
            value = value + "\\x";
            if (bytearray.length > 1) {
                value = value + Integer.toHexString((int) bytearray[i]).
                        substring(6).toUpperCase();
            } else {
                value = value + Integer.toHexString((int) bytearray[i]).
                        toUpperCase();
            }

        }
        return value;
    }

    /**
     * Generates the official Unicode name for this character.
     *   For example, if this character is a lower-case letter e with an acute
     * accent, then this method returns the string "LATIN SMALL LETTER E WITH
     * ACUTE" (without quotation marks).
     *
     * @return this character's Unicode name
     */
    public String getCharacterName() throws IOException,
            IllegalArgumentException {
        //reads unicode data from UnicodeData.txt
        BufferedReader unicodeData =
            new BufferedReader(new
                    FileReader("./edu/carleton/youj/UnicodeData.txt"));
        String codePointString = this.toCodePointString().substring(2);
        //iterate through lines of file until we find matching codepoint.
        String nextLine = null;
        while ((nextLine = unicodeData.readLine()) != null) {
            if (nextLine.substring(0,
                    codePointString.length()).equals(codePointString)) {
                String[] lineArray = nextLine.split(";");
                //check if character is a control char.
                if(lineArray[1].equals("<control>")){
                    try {
                        //check if control char has extra information.
                        return "<control> " + lineArray[10];
                    } catch(ArrayIndexOutOfBoundsException e){
                        return lineArray[1];
                    }
                } else {
                    return lineArray[1];
                }
            }
        }
        //if codepoint is unassigned, return a string saying that it is
        //unassigned.
        return "<unknown>" + this.toCodePointString();
    }
}

//yay, we're done!