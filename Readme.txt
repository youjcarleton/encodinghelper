Created by Josh You (Youj) and Aditya Subramanian (Subramaniana).
Cannot be distributed without the express written consent of the NFL.
Unique feature is that it can interpret a user's input, even if they don't
specify what it is.
For example, you could say Java EncodingHelper U+0041, and the code would
interpret the input as a unicode codepoint character and return a summary of the
capital letter A.
You could also input Java EnncodingHelper -o utf8 U+0041 and the output would be
"\x41".
You may still also specify an input, however if your input format doesn't match
your input, the program will yell at you.
Everything else works as specified (as far as we know. If it doesn't, well,
that's your job to figure out).

Stay fresh, Mr. Teach & co.

- Adi and Josh